import java.net.URI;
import java.net.URISyntaxException;

import COAPKerbDomain.AuthenticationServer.ASResponse;
import COAPKerbDomain.AuthenticationServer.ClientCredentials;
import COAPKerbDomain.AuthenticationServer.ClientTGTRequest;
import COAPKerbDomain.TicketGrantingServer.TGSRequest;
import COAPKerbDomain.TicketGrantingServer.TGSRequestContent;
import COAPKerbDomain.TicketGrantingServer.TGSResponse;
import COAPKerbDomain.TicketGrantingServer.TGSResponseContent;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;

public class Client {

    private static String AuthServerURI = "coap://192.168.1.121:5683/Authenticate";
    private static String TGSServerURI  = "coap://192.168.1.121:5683/TicketGrantingService";

    private static String password = "iotlab1@";
    private static String userName = "user1";
    private static String dummyIP  = "192.168.1.1";

    public static void main(String args[]) {
        URI authServerURI = null;
        URI tgsURI = null;

        try {
            authServerURI = new URI(AuthServerURI);
            tgsURI = new URI(TGSServerURI);
        } catch (URISyntaxException e) {
            System.err.println("Invalid URI: " + e.getMessage());
            System.exit(-1);
        }

        //Authentication and TGT request
        CoapClient client = new CoapClient(authServerURI);
        byte[] cborData = prepareRequest();
        CoapResponse response = client.post(cborData, MediaTypeRegistry.APPLICATION_CBOR );
        client.shutdown();

        if (response!=null) {

            System.out.println(response.getCode());
            System.out.println(response.getOptions());
            requestResourceTicket(response,tgsURI);

        } else {
            System.out.println("No response received.");
        }
    }

    private static void requestResourceTicket(CoapResponse response,URI tgsURI) {

        System.out.println(response.getResponseText());

        TGSRequestContent tgsReqContent = new TGSRequestContent();
        TGSRequest tgsReq = new TGSRequest();

        try {
            ASResponse authServerResponse = ASResponse.toObject(response.getPayload());
            ClientCredentials clientCred = authServerResponse.getClientCredential(password);

            tgsReqContent.resource="camera";
            tgsReqContent.encryptedCredential = clientCred.encryptedCredential;

            tgsReq.setContent(tgsReqContent,clientCred.tgsSessionKey);

            CoapClient client = new CoapClient(tgsURI);
            CoapResponse tgsCoapResponse = client.post(tgsReq.toCBOR(),MediaTypeRegistry.APPLICATION_CBOR);

            TGSResponse tgsResponse = TGSResponse.toObject(tgsCoapResponse.getPayload());
            TGSResponseContent tgsResponseContent = tgsResponse.getContent(clientCred.tgsSessionKey);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static byte[] prepareRequest() {
        ClientTGTRequest utr = new ClientTGTRequest(userName);
        byte[] cborData = null;
        try {
            utr.setUserSignature(dummyIP,password);
            cborData= utr.toCBOR();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cborData;
    }
}